#PE

# typedef struct _IMAGE_FILE_HEADER {
#     USHORT  Machine;
#     USHORT  NumberOfSections;
#     ULONG   TimeDateStamp;
#     ULONG   PointerToSymbolTable;
#     ULONG   NumberOfSymbols;
#     USHORT  SizeOfOptionalHeader;
#     USHORT  Characteristics;
# } IMAGE_FILE_HEADER, *PIMAGE_FILE_HEADER;

#optional header

# typedef struct _IMAGE_OPTIONAL_HEADER {
#     USHORT  Magic;
#     UCHAR   MajorLinkerVersion;
#     UCHAR   MinorLinkerVersion;
#     ULONG   SizeOfCode;
#     ULONG   SizeOfInitializedData;
#     ULONG   SizeOfUninitializedData;
#     ULONG   AddressOfEntryPoint;
#     ULONG   BaseOfCode;
#     ULONG   BaseOfData;
#     ULONG   ImageBase;
#     ULONG   SectionAlignment;
#     ULONG   FileAlignment;
#     USHORT  MajorOperatingSystemVersion;
#     USHORT  MinorOperatingSystemVersion;
#     USHORT  MajorImageVersion;
#     USHORT  MinorImageVersion;
#     USHORT  MajorSubsystemVersion;
#     USHORT  MinorSubsystemVersion;
#     ULONG   Reserved1;
#     ULONG   SizeOfImage;
#     ULONG   SizeOfHeaders;
#     ULONG   CheckSum;
#     USHORT  Subsystem;
#     USHORT  DllCharacteristics;
#     ULONG   SizeOfStackReserve;
#     ULONG   SizeOfStackCommit;
#     ULONG   SizeOfHeapReserve;
#     ULONG   SizeOfHeapCommit;
#     ULONG   LoaderFlags;
#     ULONG   NumberOfRvaAndSizes;
#     IMAGE_DATA_DIRECTORY DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
# } IMAGE_OPTIONAL_HEADER, *PIMAGE_OPTIONAL_HEADER;

#tables

# typedef struct _IMAGE_DATA_DIRECTORY {
#     ULONG   VirtualAddress;
#     ULONG   Size;
# } IMAGE_DATA_DIRECTORY, *PIMAGE_DATA_DIRECTORY;

#section header

# typedef struct _IMAGE_SECTION_HEADER {
#     UCHAR   Name[IMAGE_SIZEOF_SHORT_NAME];
#     union {
#             ULONG   PhysicalAddress;
#             ULONG   VirtualSize;
#     } Misc;
#     ULONG   VirtualAddress;
#     ULONG   SizeOfRawData;
#     ULONG   PointerToRawData;
#     ULONG   PointerToRelocations;
#     ULONG   PointerToLinenumbers;
#     USHORT  NumberOfRelocations;
#     USHORT  NumberOfLinenumbers;
#     ULONG   Characteristics;
# } IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;

#export table

# typedef struct _IMAGE_EXPORT_DIRECTORY {
#     ULONG   Characteristics;
#     ULONG   TimeDateStamp;
#     USHORT  MajorVersion;
#     USHORT  MinorVersion;
#     ULONG   Name;
#     ULONG   Base;
#     ULONG   NumberOfFunctions;
#     ULONG   NumberOfNames;
#     PULONG  *AddressOfFunctions;
#     PULONG  *AddressOfNames;
#     PUSHORT *AddressOfNameOrdinals;
# } IMAGE_EXPORT_DIRECTORY, *PIMAGE_EXPORT_DIRECTORY;

#import table

# typedef struct tagImportDirectory
#     {
#     DWORD    dwRVAFunctionNameList;
#     DWORD    dwUseless1;
#     DWORD    dwUseless2;
#     DWORD    dwRVAModuleName;
#     DWORD    dwRVAFunctionAddressList;
#     }IMAGE_IMPORT_MODULE_DIRECTORY,
#      * PIMAGE_IMPORT_MODULE_DIRECTORY;


with open("C:\RevEng_shared/file.exe", "rb") as f:
    byte = f.read(1)
    i = 0
    str = ""
    offset = 0
    sections = 0
    ImportRVA = 0
    ImportRVAvisak = 0
    ExportRVA = 0
    ExportRVAvisak = 0
    PhysicalOffsetImport = 0
    PhysicalOffsetExport = 0
    flagImport = 0
    flagExport = 0
    PointerToRawDataImport = 0
    PointerToRawDataExport = 0
    ImportNameTableRVAimport = 0
    ImportNameTableRVAexport = 0
    zastava2 = 0
    offsetThunk = 0
    first1 = 0
    first2 = 0
    first3 = 0
    first4 = 0
    first5 = 0
    cnt1 = 0
    offsetImport = 0
    offsetExport = 0
    br1 = 0
    finalString = ""
    sectionCnt = 0
    sectionRVAimport = 0
    sectionRVAexport = 0
    addressTableRVA = 0
    namPointerTableRVA = 0
    ordinalTableRVA = 0
    numberOfFunctions = 0
    numberOfNames = 0
    addressTableRVAoffset = 0
    addressTableRVAcnt = 0
    namPointerTableRVAcnt = 0
    namPointerTableRVAoffset = 0
    ordinalTableRVAcnt = 0
    ordinalTableRVAoffset = 0

    while byte:
        if zastava2 == 0:
            #MZ
            if i == 0:
                str += hex(ord(byte))
                print("MZ header")
                print("=========")
            if i == 1:
                str = hex(ord(byte)) + str[2:]
                print("Signature: %s"%str)
                str = ""
            if i == 2:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 3:
                str =  "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("BytesOnLastPageOfFile: %s"%str)
                str = ""
            if i == 4:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 5:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("PagesInFile: %s" % str)
                str = ""
            if i == 6:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 7:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("Relocations: %s" % str)
                str = ""
            if i == 8:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 9:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("SizeOfHeaderInParagraphs: %s" % str)
                str = ""
            if i == 10:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 11:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("MinimumExtraParagraphs: %s" % str)
                str = ""
            if i == 12:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 13:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("MaximumExtraParagraphs: %s" % str)
                str = ""
            if i == 14:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 15:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("Initial(relative)SS: %s" % str)
                str = ""
            if i == 16:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 17:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("InitialSP: %s" % str)
                str = ""
            if i == 18:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 19:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("Checksum: %s" % str)
                str = ""
            if i == 20:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 21:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("InitialIP: %s" % str)
                str = ""
            if i == 22:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 23:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("Initial(relatve)CS: %s" % str)
                str = ""
            if i == 24:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 25:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("OffsetToRelocationTable: %s" % str)
                str = ""
            if i == 26:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 27:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("OverlayNumber: %s" % str)
                str = ""
            if i == 34:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 35:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("OEMIdentifier: %s" % str)
                str = ""
            if i == 36:
                str += hex(ord(byte))[2:].zfill(2)
            if i == 37:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                print("OEMInformation: %s" % str)
                str = ""
            if i == 60:
                str += hex(ord(byte))[2:].zfill(2)
                offset = ord(byte)
            if i == 61:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                offset = ord(byte)*256 + offset
                print("OffsetToNewEXEHeader: %s" % str)
                str = ""

            #PE
            if i > 61:
                if i == offset:
                    str += hex(ord(byte))[2:].zfill(2)
                    print("     ")
                    print("PE header")
                    print("=========")
                if i == offset + 1:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 2:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 3:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Signature: %s" % str)
                    str = ""
                if i == offset + 4:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 5:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Machine: %s" % str)
                    str = ""
                if i == offset + 6:
                    str += hex(ord(byte))[2:].zfill(2)
                    sections = ord(byte)
                if i == offset + 7:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    sections = ord(byte)*256 + sections
                    print("NumberOfSections: %s" % str)
                    str = ""
                if i == offset + 8:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 9:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 10:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 11:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("TimeDateStamp: %s" % str)
                    str = ""
                if i == offset + 12:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 13:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 14:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 15:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("PointerToSymbolTable: %s" % str)
                    str = ""
                if i == offset + 16:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 17:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 18:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 19:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("NumberOfSymbols: %s" % str)
                    str = ""
                if i == offset + 20:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 21:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfOptionalHeader: %s" % str)
                    str = ""
                if i == offset + 22:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 23:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Characteristics: %s" % str)
                    str = ""

                #optional
                if i == offset + 24:
                    print("     ")
                    print("Optional header")
                    print("===============")
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 25:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Magic: %s" % str)
                    str = ""
                if i == offset + 26:
                    str = "0x" + hex(ord(byte))[2:].zfill(2)
                    print("MajorLinkerVersion: %s" % str)
                    str = ""
                if i == offset + 27:
                    str = "0x" + hex(ord(byte))[2:].zfill(2)
                    print("MinorLinkerVersion: %s" % str)
                    str = ""
                if i == offset + 28:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 29:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 30:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 31:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfCode: %s" % str)
                    str = ""
                if i == offset + 32:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 33:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 34:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 35:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfInitializedData: %s" % str)
                    str = ""
                if i == offset + 36:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 37:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 38:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 39:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfUninitializedData: %s" % str)
                    str = ""
                if i == offset + 40:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 41:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 42:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 43:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("AddressOfEntryPoint: %s" % str)
                    str = ""
                if i == offset + 44:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 45:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 46:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 47:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("BaseOfCode: %s" % str)
                    str = ""
                if i == offset + 48:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 49:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 50:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 51:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("BaseOfData: %s" % str)
                    str = ""
                if i == offset + 52:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 53:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 54:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 55:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("ImageBase: %s" % str)
                    str = ""
                if i == offset + 56:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 57:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 58:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 59:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SectionAlignment: %s" % str)
                    str = ""
                if i == offset + 60:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 61:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 62:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 63:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("FileAlignment: %s" % str)
                    str = ""
                if i == offset + 64:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 65:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("MajorO/SVersion: %s" % str)
                    str = ""
                if i == offset + 66:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 67:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("MinorO/SVersion: %s" % str)
                    str = ""
                if i == offset + 68:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 69:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("MajorImageVersion: %s" % str)
                    str = ""
                if i == offset + 70:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 71:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("MinorImageVersion: %s" % str)
                    str = ""
                if i == offset + 72:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 73:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("MajorSubsystemVersion: %s" % str)
                    str = ""
                if i == offset + 74:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 75:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("MinorSubsystemVesrion: %s" % str)
                    str = ""
                if i == offset + 76:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 77:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 78:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 79:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Win32VersionValue: %s" % str)
                    str = ""
                if i == offset + 80:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 81:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 82:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 83:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfImage: %s" % str)
                    str = ""
                if i == offset + 84:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 85:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 86:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 87:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfHeaders: %s" % str)
                    str = ""
                if i == offset + 88:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 89:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 90:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 91:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Checksum: %s" % str)
                    str = ""
                if i == offset + 92:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 93:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Subsystem: %s" % str)
                    str = ""
                if i == offset + 94:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 95:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("DLLCharacteristics: %s" % str)
                    str = ""
                if i == offset + 96:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 97:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 98:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 99:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfStackReserve: %s" % str)
                    str = ""
                if i == offset + 100:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 101:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 102:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 103:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfStackCommit: %s" % str)
                    str = ""
                if i == offset + 104:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 105:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 106:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 107:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfHeapReserve: %s" % str)
                    str = ""
                if i == offset + 108:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 109:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 110:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 111:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("SizeOfHeapCommit: %s" % str)
                    str = ""
                if i == offset + 112:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 113:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 114:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 115:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("LoaderFlags: %s" % str)
                    str = ""
                if i == offset + 116:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 117:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 118:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 119:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("NumberOfDataDirectories: %s" % str)
                    str = ""
                if i == offset + 120:
                    str += hex(ord(byte))[2:].zfill(2)
                    ExportRVAvisak = ord(byte)
                if i == offset + 121:
                    str = hex(ord(byte))[2:].zfill(2) + str
                    ExportRVA = ord(byte)*256 + ExportRVAvisak
                if i == offset + 122:
                    ExportRVA = ord(byte)*256*256 + ExportRVA
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 123:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Export table RVA: %s" % str)
                    str = ""
                if i == offset + 124:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 125:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 126:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 127:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Export table size: %s" % str)
                    str = ""
                if i == offset + 128:
                    str += hex(ord(byte))[2:].zfill(2)
                    ImportRVAvisak = ord(byte)
                if i == offset + 129:
                    str = hex(ord(byte))[2:].zfill(2) + str
                    ImportRVA = ord(byte)*256 + ImportRVAvisak
                if i == offset + 130:
                    str = hex(ord(byte))[2:].zfill(2) + str
                    ImportRVA = ord(byte)*256*256 + ImportRVA
                if i == offset + 131:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Import table RVA: %s" % str)
                    str = ""
                if i == offset + 132:
                    str += hex(ord(byte))[2:].zfill(2)
                if i == offset + 133:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 134:
                    str = hex(ord(byte))[2:].zfill(2) + str
                if i == offset + 135:
                    str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                    print("Import table size: %s" % str)
                    str = ""

                #sections
                if sections > sectionCnt:
                    if i == offset + 248:
                        print("     ")
                        if sectionCnt == 0:
                            print("Section header")
                            print("==============")
                        str += byte.decode()
                    if i == offset + 249 and ord(byte) != 0:
                        str += byte.decode()
                    if i == offset + 250 and ord(byte) != 0:
                        str += byte.decode()
                    if i == offset + 251 and ord(byte) != 0:
                        str += byte.decode()
                    if i == offset + 252 and ord(byte) != 0:
                        str += byte.decode()
                    if i == offset + 253 and ord(byte) != 0:
                        str += byte.decode()
                    if i == offset + 254 and ord(byte) != 0:
                        str += byte.decode()
                    if i == offset + 255:
                        str += byte.decode()
                        print("Name: %s" % str)
                        str = ""
                    if i == offset + 256:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 257:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 258:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 259:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("Virtual size: %s" % str)
                        str = ""
                    if i == offset + 260:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 261:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        sectionRVA = ord(byte)*256
                    if i == offset + 262:
                        sectionRVA = ord(byte) * 256 * 256 + sectionRVA
                        str = hex(ord(byte))[2:].zfill(2) + str
                        ImportRVAmasked = ImportRVA & 0xFFFF000
                        ExportRVAmasked = ExportRVA & 0xFFFF000
                        if sectionRVA == ImportRVAmasked:
                            flagImport = 1
                        if sectionRVA == ExportRVAmasked:
                            flagExport = 1
                    if i == offset + 263:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("RVA: %s" % str)
                        str = ""
                    if i == offset + 264:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 265:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 266:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 267:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("SizeOfRawData: %s" % str)
                        str = ""
                    if i == offset + 268:
                        str += hex(ord(byte))[2:].zfill(2)
                        if flagImport == 1:
                            PointerToRawDataImport = ord(byte)
                        if flagExport == 1:
                            PointerToRawDataExport = ord(byte)
                    if i == offset + 269:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        if flagImport == 1:
                            PointerToRawDataImport = ord(byte) * 256 + PointerToRawDataImport
                        if flagExport == 1:
                            PointerToRawDataExport = ord(byte) * 256 + PointerToRawDataExport
                    if i == offset + 270:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        if flagImport == 1:
                            PointerToRawDataImport = ord(byte) * 256 * 256 + PointerToRawDataImport
                            PhysicalOffsetImport = PointerToRawDataImport + ImportRVA - sectionRVA
                            flagImport = 0
                            sectionRVAimport = sectionRVA
                        if flagExport == 1:
                            PointerToRawDataExport = ord(byte) * 256 * 256 + PointerToRawDataExport
                            PhysicalOffsetExport = PointerToRawDataExport + ExportRVA - sectionRVA
                            sectionRVAexport = sectionRVA
                            flagExport = 0
                    if i == offset + 271:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("PointerToRawData: %s" % str)
                        str = ""
                    if i == offset + 272:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 273:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 274:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 275:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("PoinetToRelocations: %s" % str)
                        str = ""
                    if i == offset + 276:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 277:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 278:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 279:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("PointerToLineNumbers: %s" % str)
                        str = ""
                    if i == offset + 280:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 281:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("NumberOfRelocations: %s" % str)
                        str = ""
                    if i == offset + 282:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 283:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("NumberOfLineNumbers: %s" % str)
                        str = ""
                    if i == offset + 284:
                        str += hex(ord(byte))[2:].zfill(2)
                    if i == offset + 285:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 286:
                        str = hex(ord(byte))[2:].zfill(2) + str
                    if i == offset + 287:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        print("Characteristics: %s" % str)
                        str = ""
                        sectionCnt += 1
                        if sections > sectionCnt:
                            offset += 40
                #import table
                else:
                    if i == PhysicalOffsetImport and first2 == 0:
                        print("     ")
                        print("Import table")
                        print("============")
                        print("     ")
                        print("Import directory")
                        print("================")
                        str += hex(ord(byte))[2:].zfill(2)
                        finalString += hex(ord(byte))[2:].zfill(2)
                        if first2 == 0:
                            ImportNameTableRVAimport = ord(byte)
                    elif i == PhysicalOffsetImport + offsetImport:
                        print("     ")
                        print("Import directory")
                        print("================")
                        str += hex(ord(byte))[2:].zfill(2)
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 1 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                        if first2 == 0:
                            ImportNameTableRVAimport = ord(byte) * 256 + ImportNameTableRVAimport
                    if i == PhysicalOffsetImport + 2 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 3 + offsetImport:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                        print("ImportNameTableRVA: %s (Physical: %s)" % (str, hex(PhysicalOffsetImport + offsetImport)))
                        if first2 == 0:
                            ImportNameTableRVAimport = ImportNameTableRVAimport - sectionRVAimport + PointerToRawDataImport
                        str = ""
                    if i == PhysicalOffsetImport + 4 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 5 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 6 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 7 + offsetImport:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                        print("TimeDateStamp: %s (Physical: %s)" % (str, hex(PhysicalOffsetImport + 4 + offsetImport)))
                        str = ""
                    if i == PhysicalOffsetImport + 8 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 9 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 10 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 11 + offsetImport:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                        print("ForwarderChain: %s (Physical: %s)" % (str, hex(PhysicalOffsetImport + 8 + offsetImport)))
                        str = ""
                    if i == PhysicalOffsetImport + 12 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 13 + offsetImport:

                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 14 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 15 + offsetImport:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                        print("NameRVA: %s (Physical: %s)" % (str, hex(PhysicalOffsetImport + 12 + offsetImport)))
                        str = ""
                    if i == PhysicalOffsetImport + 16 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 17 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 18 + offsetImport:
                        str = hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                    if i == PhysicalOffsetImport + 19 + offsetImport:
                        str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                        finalString += hex(ord(byte))[2:].zfill(2)
                        if finalString != "0000000000000000000000000000000000000000":
                            print("ImportAddressTableRVA: %s (Physical: %s)" % (str, hex(PhysicalOffsetImport + 16 + offsetImport)))
                            br1 += 1
                        else:
                            zastava2 = 1
                        str = ""
                        offsetImport += 20
                        finalString = ""
                        first2 = 1

                    #export table
                    if PhysicalOffsetExport != 0:
                        if i == PhysicalOffsetExport:
                            print("     ")
                            print("Export table")
                            print("============")
                            print("     ")
                            print("Export directory")
                            print("================")
                            str += hex(ord(byte))[2:].zfill(2)
                        if i == PhysicalOffsetExport + 1:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 2:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 3:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("Characteristics: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport)))
                            str = ""
                        if i == PhysicalOffsetExport + 4:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 5:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 6:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 7:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("TimeDateStamp: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 4)))
                            str = ""
                        if i == PhysicalOffsetExport + 8:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 9:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("MajorVersion: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 6)))
                            str = ""
                        if i == PhysicalOffsetExport + 10:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 11:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            finalString += hex(ord(byte))[2:].zfill(2)
                            print("MinorVersion: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 8)))
                            str = ""
                        if i == PhysicalOffsetExport + 12:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 13:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 14:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 15:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("NameRVA: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 12)))
                            str = ""
                        if i == PhysicalOffsetExport + 16:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 17:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 18:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 19:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("OrdinalBase: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 16)))
                            str = ""
                        if i == PhysicalOffsetExport + 20:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            numberOfFunctions = ord(byte)
                        if i == PhysicalOffsetExport + 21:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 22:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 23:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("NumberOfFunctions: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 20)))
                            str = ""
                        if i == PhysicalOffsetExport + 24:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            numberOfNames = ord(byte)
                        if i == PhysicalOffsetExport + 25:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 26:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 27:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("NumberOfNames: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 24)))
                            str = ""
                        if i == PhysicalOffsetExport + 28:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            addressTableRVA = ord(byte)
                        if i == PhysicalOffsetExport + 29:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            addressTableRVA = ord(byte) * 256 + addressTableRVA
                        if i == PhysicalOffsetExport + 30:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 31:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            print("AddressTableRVA: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 28)))
                            addressTableRVA = addressTableRVA - sectionRVAexport + PointerToRawDataExport
                            str = ""
                        if i == PhysicalOffsetExport + 32:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            namPointerTableRVA = ord(byte)
                        if i == PhysicalOffsetExport + 33:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            namPointerTableRVA = ord(byte) * 256 + namPointerTableRVA
                        if i == PhysicalOffsetExport + 34:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 35:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            namPointerTableRVA = namPointerTableRVA - sectionRVAexport + PointerToRawDataExport
                            print("NamePointerTableRVA: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 32)))
                            str = ""
                        if i == PhysicalOffsetExport + 36:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            ordinalTableRVA = ord(byte)
                        if i == PhysicalOffsetExport + 37:
                            str = hex(ord(byte))[2:].zfill(2) + str
                            ordinalTableRVA = ord(byte) * 256 + ordinalTableRVA
                        if i == PhysicalOffsetExport + 38:
                            str = hex(ord(byte))[2:].zfill(2) + str
                        if i == PhysicalOffsetExport + 39:
                            str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                            ordinalTableRVA = ordinalTableRVA - sectionRVAexport + PointerToRawDataExport
                            print("OrdinalTableRVA: %s (Physical: %s)" % (str, hex(PhysicalOffsetExport + 36)))
                            str = ""

                        #export address table
                        if addressTableRVAcnt < numberOfFunctions:
                            if i == addressTableRVA + addressTableRVAoffset and first3 == 0:
                                print("     ")
                                print("Export address table")
                                print("====================")
                                str = hex(ord(byte))[2:].zfill(2) + str
                            elif i == addressTableRVA + addressTableRVAoffset:
                                str = hex(ord(byte))[2:].zfill(2) + str
                            if i == addressTableRVA + 1 + addressTableRVAoffset:
                                str = hex(ord(byte))[2:].zfill(2) + str
                            if i == addressTableRVA + 2 + addressTableRVAoffset:
                                str = hex(ord(byte))[2:].zfill(2) + str
                            if i == addressTableRVA + 3 + addressTableRVAoffset:
                                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                                print("FunctionRVA: %s (Physical: %s)" % (str, hex(addressTableRVA + addressTableRVAoffset)))
                                str = ""
                                addressTableRVAoffset += 4
                                addressTableRVAcnt += 1
                                first3 = 1

                        #export name table
                        if namPointerTableRVAcnt < numberOfNames:
                            if i == namPointerTableRVA + namPointerTableRVAoffset and first4 == 0:
                                print("     ")
                                print("Export name table")
                                print("=================")
                                str = hex(ord(byte))[2:].zfill(2) + str
                            elif i == namPointerTableRVA + namPointerTableRVAoffset:
                                str = hex(ord(byte))[2:].zfill(2) + str
                            if i == namPointerTableRVA + namPointerTableRVAoffset + 1:
                                str = hex(ord(byte))[2:].zfill(2) + str
                            if i == namPointerTableRVA + namPointerTableRVAoffset + 2:
                                str = hex(ord(byte))[2:].zfill(2) + str
                            if i == namPointerTableRVA + namPointerTableRVAoffset + 3:
                                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                                print("FunctionNameRVA: %s (Physical: %s)" % (str, hex(namPointerTableRVA + namPointerTableRVAoffset)))
                                str = ""
                                namPointerTableRVAoffset += 4
                                namPointerTableRVAcnt += 1
                                first4 = 1

                        #export ordinal table
                        if ordinalTableRVAcnt < numberOfNames:
                            if i == ordinalTableRVA + ordinalTableRVAoffset and first5 == 0:
                                print("     ")
                                print("Export ordinal table")
                                print("====================")
                                str = hex(ord(byte))[2:].zfill(2) + str
                            elif i == ordinalTableRVA + ordinalTableRVAoffset:
                                str = hex(ord(byte))[2:].zfill(2) + str
                            if i == ordinalTableRVA + ordinalTableRVAoffset + 1:
                                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                                print("FunctionNameRVA: %s (Physical: %s)" % (str, hex(ordinalTableRVA + ordinalTableRVAoffset)))
                                str = ""
                                ordinalTableRVAoffset += 2
                                ordinalTableRVAcnt += 1
                                first5 = 1

        #thunks
        elif cnt1 != br1 and zastava2 == 1:

            if i == ImportNameTableRVAimport and first1 == 0:
                print("     ")
                print("Import Thunks")
                print("=============")
                str = hex(ord(byte))[2:].zfill(2) + str
            elif i == ImportNameTableRVAimport + offsetThunk:
                str = hex(ord(byte))[2:].zfill(2) + str
            if i == ImportNameTableRVAimport + 1 + offsetThunk:
                str = hex(ord(byte))[2:].zfill(2) + str
            if i == ImportNameTableRVAimport + 2 + offsetThunk:
                str = hex(ord(byte))[2:].zfill(2) + str
            if i == ImportNameTableRVAimport + 3 + offsetThunk:
                str = "0x" + hex(ord(byte))[2:].zfill(2) + str
                if str != "0x00000000":
                    print("Hint/NameRVA: %s (Physical: %s)" % (str, hex(ImportNameTableRVAimport + offsetThunk)))
                else:
                    cnt1 += 1
                str = ""
                offsetThunk += 4
                first1 = 1

        byte = f.read(1)
        i += 1